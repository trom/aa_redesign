# {{ exchange-title[Exchange title text] The Acceptable Ads Exchange (AAX) }}

{{ exchange-body[Exchange body text] A programmatic ad exchange that allows  

publishers and advertisers to access the  

Acceptable Ads inventory. }}

[{{ exchange-read-more-label[Exchange button text] Find out more }}](/en/solutions){: .btn-primary }
