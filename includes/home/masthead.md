# {{ masthead-heading-1[Masthead heading line 1] Building bridges. }}

{{ masthead-body[Masthead body text] Great things happen when people cooperate. }}

{{ masthead-body-1[Masthead body-1 text] Acceptable Ads allow publishers, advertisers, and ad-blocking users to work together. Working together creates a better, more sustainable ecosystem for everyone. }}

{{ masthead-body-2[Masthead body-2 text] Together we can make online advertising better. Join the Acceptable Ads initiative and reach new users. }}

[{{ read-more-label[button text] Read more }}](#advantages){: .btn-primary }
